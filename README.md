CompTech_Messenger
---
The CompTech_Messenger is a cross-platform application for smartphones that allows
you to keep statistics of visits and conduct surveys. There is an opportunity to
purchase a paid account that gives the right to create lectures and surveys.
The project solves the following tasks:
- attendance registration (“I am here” button)
- polls (created by the Lector)
- statistics collection and displaying it in the organizers profiles (reviews of the
quality of lectures and assessment of students' knowledge)

At the moment, attendance records are made on paper, which entails a loss of time.
Interviews are conducted orally, statistics are not kept.

The project is a mobile application for organizing the educational process.

System requirements
---
To install and run Flutter, your development environment must meet these minimum
requirements:

Operating Systems: Linux (64-bit)
Disk Space: 600 MB (does not include disk space for IDE/tools).
Tools: Flutter depends on these command-line tools being available in your
environment.
- bash
- curl
- git 2.x
- mkdir
- rm
- unzip
- which
- xz-utils

Shared libraries: Flutter test command depends on this library being available in
your environment.
libGLU.so.1 - provided by mesa packages e.g. libglu1-mesa on Ubuntu/Debian


Installation
---
The following information concentrates on compiling on LINUX machines.

1)Get the Flutter SDK

    $ mkdir /home/user/flutter && cd /home/user/flutter

Download the following installation bundle to get the latest stable release of the
Flutter SDK:

    $ wget https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.0.0-stable.tar.xz

Extract the file in the desired location, for example:

    $ tar xf ~/Downloads/flutter_linux_v1.0.0-stable.tar.xz

Add the flutter tool to your path:

    $ export PATH="$PATH:/home/user/flutter/bin"
    
This command sets your PATH variable for the current terminal window only.

    $ flutter doctor
This command checks your environment and displays a report to the terminal window.
The Dart SDK is bundled with Flutter; it is not necessary to install Dart separately.
Check the output carefully for other software you may need to install or further
tasks to perform.

Once you have installed any missing dependencies, run the flutter doctor command
again to verify that you’ve set everything up correctly.

Determine the directory where you placed the Flutter SDK.

Open (or create) $HOME/.bash_profile. The file path and filename might be different
on your machine.

Add the following line and change [PATH_TO_FLUTTER_GIT_DIRECTORY] to be
the path where you cloned Flutter’s git repo:

    export PATH="$PATH:[PATH_TO_FLUTTER_GIT_DIRECTORY]/flutter/bin"
 
Run source $HOME/.bash_profile to refresh the current window.
 
Verify that the flutter/bin directory is now in your PATH by running:

    echo $PATH
 
2)Android setup 
Download and install Android Studio.
Start Android Studio, and go through the ‘Android Studio Setup Wizard’. This installs
the latest Android SDK, Android SDK Platform-Tools, and Android SDK Build-Tools,
which are required by Flutter when developing for Android.

Set up your Android device
To prepare to run and test your Flutter app on an Android device, you’ll need an
Android device running Android 4.1 (API level 16) or higher.

- Enable Developer options and USB debugging on your device. Detailed instructions
are available in the Android documentation.
- Using a USB cable, plug your phone into your computer. If prompted on your device,
authorize your computer to access your device.
- In the terminal, run the flutter devices command to verify that Flutter recognizes
your connected Android device.
- By default, Flutter uses the version of the Android SDK where your adb tool is
based. If you want Flutter to use a different installation of the Android SDK, you
must set the ANDROID_HOME environment variable to that installation directory.

Set up the Android emulator

To prepare to run and test your Flutter app on the Android emulator, follow these
steps:

- Enable VM acceleration on your machine.
- Launch Android Studio > Tools > Android > AVD Manager and select Create Virtual
Device. (The Android submenu is only present when inside an Android project.)
- Choose a device definition and select Next.
- Select one or more system images for the Android versions you want to emulate,
and select Next. An x86 or x86_64 image is recommended.
- Under Emulated Performance, select Hardware - GLES 2.0 to enable hardware
acceleration.
- Verify the AVD configuration is correct, and select Finish.

For details on the above steps, see Managing AVDs.

In Android Virtual Device Manager, click Run in the toolbar. The emulator starts
up and displays the default canvas for your selected OS version and device.


Useful links
---
- [Graphql](https://graphql.org/learn/)
- [Flutter](https://flutter.io/docs/get-started/install/linux)
- [Android](https://developer.android.com/guide/)

Contacts
---
- asromahin789@gmail.ru
- petrenko.mixa@gmail.com
- andreyevart@gmail.com

License
---
Copyright (c) 2019 <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
````