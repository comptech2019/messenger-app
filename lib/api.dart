import 'dart:convert';
import 'package:http/http.dart';
import 'package:http_auth/http_auth.dart';
import 'routes/channels.dart';
import 'package:flutter/material.dart';
import 'routes/user.dart';
import 'routes/dialog_view.dart';
import 'routes/DialogData.dart';
import 'dart:convert';

// TODO move to config
const apiBaseURL = 'http://tsvbits.com:4200/api';
String apiToken = '';
User currentUser = new User(login: 'no login', name: 'Gordon Freeman', role: User.ROLE_ORG, userAvatar: new Image(image: AssetImage('assets/images/avatar_pch.png'), width: 80,height: 80,));

abstract class AuthStateListener {
  void onChanged(bool isLoggedIn);
}

// TODO revise auth state management
class AuthState {
  String apiToken = '';
  List<AuthStateListener> _subscribers = new List<AuthStateListener>();

  void subscribe(AuthStateListener listener) {
    _subscribers.add(listener);
  }

  void notify(bool isLoggedIn) {
    if (!isLoggedIn) {
      apiToken = '';
    }
    _subscribers.forEach((AuthStateListener s) => s.onChanged(isLoggedIn));
  }
}

var authState = new AuthState();

void setToken(String token) {
  authState.apiToken = token;
}

String makeApiURL(String path) {
  return apiBaseURL + path;
}

Future<String> login(String username, String password) async {
  var http = BasicAuthClient(username, password);
  var res = await http.post(makeApiURL('/login'));
  var json = jsonDecode(res.body);
  var apiToken = json['token'] as String;
  setToken(apiToken);
  return apiToken;
}

Map<String, String> makeHeaders(String contentType) {
  return
    {
    'Authorization': 'Bearer ' + authState.apiToken,
    'Content-Type': contentType,
    };
}

dynamic handleResponse(Response resp) {
  if (resp.statusCode == 401) {
    authState.notify(false);
    throw new StateError('bad auth');
  }
  var respText = utf8.decode(resp.bodyBytes);
  var results = jsonDecode(respText);
  return results;
}

Future<dynamic> query(String query) async {
  var headers = makeHeaders('application/graphql');
  var resp = await post(makeApiURL('/query'), headers: headers, body: query);
  return handleResponse(resp);
}

Future<List<MessageItem>> getDialogMsgs(DialogItem dd) async
{
  var resp = await query('{list(func: has(_message)) @cascade {count(from) text created_at uid to @filter(uid(${dd.uid})) {uid } from{ name}}}');
  var items = resp['list'] as List<dynamic>;
  return items.map((m) => MessageItem.parseServerJson(m)).toList();
}
//===================================

Future<dynamic> apiPost(String path, dynamic data) async {
  var headers = makeHeaders('application/json');
  var resp = await post(makeApiURL(path), headers: headers, body: jsonEncode(data));
  return handleResponse(resp);
}

Future<dynamic> apiPut(String path, dynamic data) async {
  var headers = makeHeaders('application/json');
  var resp = await put(makeApiURL(path), headers: headers, body: jsonEncode(data));
  return handleResponse(resp);
}


Map<String, dynamic> makeDialogMap(DialogItem dd) {
  return {
    'name': dd.dialogName,
    'description': dd.lastMsgSender,
    'member': {"uid": currentUser.uid},
  };
}

Future<DialogItem> postDialog(DialogItem dialog) async
{
  var ch = await apiPost('/data/channel', makeDialogMap(dialog));
  // List<DialogItem> di = await getDialogs();
  await apiPut('/data/user/'+currentUser.uid, {
    'member_of': {
      'uid': ch['uid']
    }
  });
}

Map<String, dynamic> makeMsgMap(MessageItem mm, String uid) {
  return {
    'text': mm.text,
    'to': {"uid": uid},
    'from': {"uid": currentUser.uid},
    'choice':'',
  };
}
Future<DialogItem> postMsg(MessageItem msg, String channelUid) async
{
  await apiPost('/data/message', makeMsgMap(msg, channelUid));
}
//===================================
Future<dynamic> apiGet(String path) async {
  var headers = makeHeaders('application/json');
  var resp = await get(makeApiURL(path), headers: headers);
  return handleResponse(resp);
}

Future<List<DialogItem>> getDialogs() async
{
    //List<DialogItem> dialogs = new List();
    var res = await apiGet('/data/channel/list');
    return res.map<DialogItem>((m)
    =>
    new DialogItem(lastMessageTime: getDate(m), dialogName: getName(m),lastMsgSender:  getDescription(m), lastMsg:'',
        dialogPicture: new Image(image: new AssetImage('assets/images/avatar3_pch.png'),
          height: 70.0,
          width: 70.0,), type: DialogItem.CHAT, uid: m['uid'] as String)).toList();

}

String getName(m)
{
  if(m['name']!=null)
    {
      String s = m['name'] as String;
      return s;
    }
    else
      {
        return'null';
      }
}

Future<User> getUserData() async
{
  var res = await apiGet('/me');
  return new User(login: res['login'] as String, name: getName(res), role: User.ROLE_ORG,
      userAvatar: new Image(image: AssetImage('assets/images/avatar2_pch.png'), width: 45, height: 45,),uid: res['uid'] as String);
}
String getDate(dynamic m)
{
  String df = m['modified_at'] as String;
  return df.substring(df.indexOf('T')+1, df.indexOf('T')+6);
}
String getDescription(dynamic m)
{
  if(m['description']!=null)
    {
      String d = m['description'] as String;
      return d;
    }
    else
      {
        return'null';
      }
}