import 'package:flutter/material.dart';

class MessageItem
{
  static String SENT='sent', RECEIVED = 'received';
  String text, senderName, sendTime, type;
  bool isQoute;
  Image senderAvatar;
  MessageItem({this.text, this.senderName, this.sendTime,this.type, this.senderAvatar, this.isQoute})
  {
  }

  MessageItem.parseServerJson(Map<String, dynamic> data) {
    text = data['text'] as String;
    var from = data['from'] as List<dynamic>;
    senderName = from[0]['name'] as String;
    isQoute = false;
  }
}

class DialogData
{
  List<MessageItem> msgs;
  String uid;
  String dialogName;
  int membersCount;
  Image chatAvatar;

  DialogData({this.msgs, this.dialogName, this.membersCount,this.chatAvatar, this.uid}){}
}