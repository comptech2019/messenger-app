import 'package:flutter/material.dart';

class AddQoute extends StatefulWidget {
  final Function() addQoute,addIAmHere;
  final String tooltip;
  final IconData icon;

  AddQoute({this.addQoute, this.addIAmHere, this.tooltip, this.icon});

  @override
  _AddQoute createState() => _AddQoute(addQoute: this.addQoute, addIAmHere: this.addIAmHere);
}

class _AddQoute extends State<AddQoute>
    with SingleTickerProviderStateMixin
{
  final Function() addQoute,addIAmHere;

  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  _AddQoute({this.addQoute, this.addIAmHere});
  @override
  initState() {
    _animationController =
    AnimationController(vsync: this, duration: Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {});
      });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));
    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget iamherebtn() {
    return Container(
      child: FloatingActionButton(heroTag: '1',
        onPressed: addIAmHere,
        tooltip: 'Add',
        child: new Icon(Icons.my_location),
      ),
    );
  }

  Widget qoute() {
    return Container(
      child: FloatingActionButton(heroTag: '2',
        onPressed: addQoute,
        tooltip: 'Image',
        child:new Icon(Icons.format_list_bulleted),
      ),
    );
  }


  Widget toggle() {
    return Container(
      child: FloatingActionButton(heroTag: '4',
          backgroundColor: _buttonColor.value,
          onPressed: animate,
          tooltip: 'Toggle',
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 2.0,
            0.0,
          ),
          child: iamherebtn(),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 1.0,
            0.0,
          ),
          child: qoute(),
        ),
        toggle(),
      ],
    );
  }
}
