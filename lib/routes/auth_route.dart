import 'package:flutter/material.dart';
import 'package:flutter_app/api.dart';
import 'package:flutter_app/routes/channels.dart';
import 'package:flutter_app/routes/register_screen.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:http_auth/http_auth.dart';
import 'package:flutter_app/routes/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginRoute extends StatefulWidget
{
  @override
  _State createState() => new _State();

}

class _State extends State<LoginRoute>
{
  String name="";
  String psw="";
  String error="";
  static Color errorColor=Colors.red[50];
  static Color notErrorColor=Colors.white;
  static Color goodColor=Colors.green[100];
  Color colorName=notErrorColor;
  Color colorPsw=notErrorColor;

  String makeApiURL(String path) {
    return apiBaseURL + path;
  }

  void register(BuildContext ctxt)
  {
    /*
    Переход на регистрацию
     */
    Navigator.push(
        ctxt,
        MaterialPageRoute(builder: (ctxt) => RegisterScreen()));
  }

  _saveTokenToDevice() async
  {

  }

  void _saveToken(BuildContext ctxt, String name, String pasw) async
  {
    apiToken = await login(name, pasw);

    if(apiToken!=null && apiToken!='')
    {
      currentUser = await getUserData();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("login", name);
      prefs.setString('password', pasw);
      await prefs.commit();
      //debugPrint(prefs.getString("token"));      currentUser = await getUserData();

      Navigator.push(
          ctxt,
          MaterialPageRoute(builder: (ctxt) => ChannelsView()));
    }
  }
  void sendAuth(BuildContext ctxt) {
    setState(()
    {
      if(name.length==0) {
        error = "Имя не введено";
        colorName=errorColor;
        colorPsw=notErrorColor;
      }
      if(psw.length==0) {
        error = "Пароль не введён";
        colorName=notErrorColor;
        colorPsw=errorColor;
      }
      if(name.length==0 && psw.length==0) {
        error = "Имя и пароль не введены";
        colorName=errorColor;
        colorPsw=errorColor;
      }
      if(name.length>0 && psw.length>0)
      {
        error = "";
        colorName=goodColor;
        colorPsw=goodColor;

        /*
        Надо получить токен, предать в окно каналов, чтобы там гонять дальше. + сохранить его данные и токен на мобиле
        */
        _saveToken(context,name, psw);

      }

    });
  }
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      
    });
    super.initState();
  }
  void nameChanged(String buf) {
    setState(() {
      name=buf;
    });
  }
  void pswChanged(String buf) {
    setState(() {
      psw=buf;
    });
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blueAccent,
        title: new Text('Авторизация',
          textAlign: TextAlign.center,
          style:  new TextStyle(
          fontSize: 20,
        ),),
      ),
      body: new Center(child: new Column(mainAxisSize: MainAxisSize.min,mainAxisAlignment: MainAxisAlignment.spaceEvenly,children:<Widget>[
        Image.asset('assets/logo.jpg',width: 150,height: 150,),
        new Container(padding: EdgeInsets.all(10),),
        new Container(
          padding:EdgeInsets.all(20.0),decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 350,
          //color: colorName,
          child: new TextField(
            onChanged: nameChanged,
            textAlign: TextAlign.center,
            style: new TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
            decoration: new InputDecoration.collapsed(
                hintText: 'Логин'
            ),
          ),
        ),
        Container(padding: EdgeInsets.all(10),),
        new Container(decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 360,
          padding:EdgeInsets.all(20.0),
          child:new TextField(
            onChanged: pswChanged,
            textAlign: TextAlign.center,
            style: new TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
            decoration: new InputDecoration.collapsed(
              hintText: 'Пароль',
            ),
              //controller: myController
          ),
        ),
        new Container(
          padding:EdgeInsets.all(20.0),
          child:new RaisedButton
            (shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
            onPressed:() => sendAuth(context),
            padding: EdgeInsets.all(20.0),
            child: new Text("Войти",
            style: new TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
          )
          ),
        new Text("$error"),
        new Container(
            padding:EdgeInsets.all(10.0),
            child:new RaisedButton
              (shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),color: Colors.blue[200],
              onPressed:() => register(context),
              padding: EdgeInsets.all(20.0),
              child: new Text("Зарегистрироваться",
                style: new TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            )
        ),

      ])),
    );
  }
}
