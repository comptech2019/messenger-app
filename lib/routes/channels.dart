import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/routes/dialog_view.dart';
import 'package:flutter_app/routes/DialogData.dart';
import 'package:flutter_app/routes/writeMsgFloatBtn.dart';
import 'package:flutter_app/routes/user.dart';
import 'dart:math';
import 'package:flutter_app/routes/profile.dart';
import 'package:flutter_app/routes/global.dart';
import 'package:flutter_app/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/routes/auth_route.dart';
import 'package:flutter_app/routes/CustomShowDialog.dart';

class ChannelsView extends StatefulWidget
{
  @override
  _State createState() => new _State();

}

//Шторка в роуте каналов, в ней добавлю аватарину, имя, и кнопки настройки, выйти,
class userDrawer extends Drawer
{

  BuildContext context;
  userDrawer({this.context});

  void _openUserProfile(BuildContext ctxt)
  {
    debugPrint('ЖМЯК');
    Navigator.push(
        ctxt,
        MaterialPageRoute(builder: (ctxt) => profileView()));
  }
  @override
  Widget build (BuildContext ctxt)
  {
    return new Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          new InkWell(onTap:() => _openUserProfile(ctxt),child: DrawerHeader(
            child: new Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Container(padding: EdgeInsets.only(right: 150),child: new Image(image: currentUser.userAvatar.image, width: 90,height: 90,)),
              Expanded(child: Container(alignment:Alignment.centerLeft,padding: EdgeInsets.only(left:15,top: 20),child: new Text(currentUser.name,style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),))),
            ],),
            decoration: BoxDecoration(
              color: Colors.grey,
            ),
          ),),

          ListTile(
            title: new Row(children: <Widget>[new Image(image: AssetImage('assets/images/tv.jpg'),width: 25,height: 25,), Padding(padding: EdgeInsets.only(top: 6),child: new Text('Channel list'))],),
            onTap: () {
              Navigator.pop(ctxt);
            },
          ),
          ListTile(
            title: new Row(children: <Widget>[new Icon(Icons.contacts), new Text('contacts')],),
            onTap: () {
              openContacts(ctxt);
              Navigator.pop(ctxt);
            },
          ),
          Padding(padding: EdgeInsets.only(top: 30),
          child: ListTile(
            title: new Row(children: <Widget>[new Icon(Icons.settings), new Text('Settings')],),
            onTap: () {
              Navigator.pop(ctxt);
            },
          )),
          ListTile(
            title: new Row(children: <Widget>[new Icon(Icons.exit_to_app), new Text('Log out')],),
            onTap: () {
              _clearPref();
              Navigator.push(
                  ctxt,
                  MaterialPageRoute(builder: (ctxt) => LoginRoute()));
              //Navigator.pop(ctxt);
            },
          ),
        ],
      ),
    );
  }

  void _clearPref() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  void openContacts(BuildContext ctxt) {}
}
class _State extends State<ChannelsView>
{

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  int count = 0;
  List<DialogItem> dialogs=new List();
  @override
  void _getData() async
  {
    dialogs = await getDialogs();

    setState(() {
      count = dialogs.length;
    });
  }
  void initState()
  {
    //debugPrint(userToken);

    setState(()
    {

      debugPrint(apiToken);
      _getData();
    });
    super.initState();
  }

  void _getMsgs()
  {

  }
  void _openDialog(int index) async
  {
    List<MessageItem> msgs = new List();
    /*for(int i=0;i<20;i++)
      {
          int rng = new Random().nextInt(100);

          if(rng.isOdd)
          {
            msgs.add( new MessageItem(text: 'message is here',
                senderName: 'Me',
                sendTime: '19:20',
                type: MessageItem.SENT,
                senderAvatar: new Image(
                  image: AssetImage('assets/images/avatar_alpha_pch.png'),
                  width: 45,
                  height: 45,), isQoute: false));
          }
          else
          {
            msgs.add(new MessageItem(text: 'message will be here',
                senderName: 'Sender',
                sendTime: '19:20',
                type: MessageItem.RECEIVED,
                senderAvatar: new Image(
                  image: AssetImage('assets/images/avatar_pch.jpg'),
                  width: 45,
                  height: 45,),isQoute: false));
          }
      }*/
    msgs = await getDialogMsgs(dialogs[index]);
    for(int i=0;i<msgs.length;i++)
      {
        msgs[i].isQoute=false;
        msgs[i].senderAvatar = new Image(
          image: AssetImage('assets/images/avatar3_pch.png'),
          width: 45,
          height: 45,);
        msgs[i].sendTime='dsa';
        if(msgs[i].senderName == currentUser.name)
          msgs[i].type = MessageItem.SENT;
        else
          msgs[i].type = MessageItem.RECEIVED;

      }

    msgs.add((new MessageItem(text: 'Question N1',
        senderName: 'Sender',
        sendTime: '19:20',
        type: MessageItem.RECEIVED,
        senderAvatar: new Image(
          image: AssetImage('assets/images/avatar_pch.jpg'),
          width: 45,
          height: 45,),isQoute: true)));
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChatRoute(data: new DialogData(dialogName: dialogs[index].dialogName,
        membersCount: 15,chatAvatar: new Image(image: dialogs[index].dialogPicture.image ,width: 45,height: 45,), msgs: msgs,uid: dialogs[index].uid),)),
    );
  }

  @override
  Widget build(BuildContext context)
  {
        return new Scaffold
          (
          drawer: new userDrawer(),
          key: _scaffoldKey,
          appBar: new AppBar
            (
            backgroundColor: Colors.blueAccent,
            leading: IconButton(icon: Icon(Icons.menu), onPressed: () => _scaffoldKey.currentState.openDrawer()),
            actions: <Widget>
            [
              new IconButton(icon: const Icon(Icons.search, color: Colors.white,), onPressed: null,),
            ],

            title: new Text('Dialogs list'),
          ),
          floatingActionButton: currentUser.role!=User.ROLE_LISTENER?new WriteMsgFloatButton(onPressed:() => addDialog(context),):null,
          body: new ListView.builder(itemCount: count,
            itemBuilder: (BuildContext context, int index)
            {
              return new InkWell(onTap:() => _openDialog(index),child: Card(
                child: new Container(
                  padding: new EdgeInsets.all(4.0),
                  //--------------------
                  child: new Row
                    (
                    children: <Widget>
                    [
                      Stack(
                          children: <Widget>[
                            dialogs[index].dialogPicture,
                            Container(padding: EdgeInsets.only(left: 15),child: Center(child: Text("", style: TextStyle(color: Colors.white, fontSize: 60),))),]),
                      //------------------
                      Container
                        (padding: EdgeInsets.only(left: 15.0),child: new Column
                        (

                        children: <Widget>
                        [
                          Container
                            (padding: EdgeInsets.only(bottom: 9),
                              child: new Row(children: <Widget>[Container(padding: EdgeInsets.only(right: 15),child: dialogs[index].typePicture),
                              new Text(dialogs[index].dialogName, style: TextStyle(fontWeight: FontWeight.bold))], )
                          ),
                          Container
                            (
                              padding: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 5),
                              decoration: BoxDecoration(
                                color: Color(0xdde6e6e6),
                                borderRadius: BorderRadius.circular(4.0),),
                              child: Row
                                (
                                children: <Widget>
                                [
                                  new Text(dialogs[index].lastMsgSender, style: TextStyle(color: Colors.blueAccent),),
                                  new Text(dialogs[index].lastMsg)
                                ],
                              )
                          ),
                        ],
                      )
                      ),
                      //-------------------
                      Container(padding: EdgeInsets.only(left: 60, bottom: 35),
                          child: new Text(dialogs[index].lastMessageTime)),
                    ],
                  ),
                  //-------------------
                ),
              ));
            }
            ,),
        );


  }

  void addDialog(BuildContext ctxt)
  {
    TextEditingController nameController = new TextEditingController();
    TextEditingController descriptionController = new TextEditingController();

    showDialog
      (
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return
          new CustomAlertDialog(
            content: new Container
              (
              width: 260.0,
              height: 330.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius:
                new BorderRadius.all(new Radius.circular(40.0)),
              ),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // dialog top
                  new Expanded(
                    child: new Row(
                      children: <Widget>[
                        new Container(alignment: Alignment.topCenter,
                           padding: new EdgeInsets.only(left: 40.0),
                          decoration: new BoxDecoration(
                            color: Colors.white,
                          ),
                          child: new Text(
                            'Создание диалога',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontFamily: 'helvetica_neue_light',
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(alignment: Alignment.topCenter,
                    padding:EdgeInsets.all(20.0),decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 350,
                    //color: colorName,
                    child: new TextField(
                      onChanged: null,
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),controller: nameController,
                      decoration: new InputDecoration.collapsed(
                          hintText: 'Название'
                      ),
                    ),
                  ),
                  Container(padding: EdgeInsets.all(10),),
                  new Container(alignment: Alignment.topCenter,
                    padding:EdgeInsets.all(20.0),decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 350,
                    //color: colorName,
                    child: new TextField(
                      onChanged: null,
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),controller: descriptionController,
                      decoration: new InputDecoration.collapsed(
                          hintText: 'Краткое описание'
                      ),
                    ),
                  ),

                  Container(padding: EdgeInsets.all(20),),
                  new Container(
                      padding:EdgeInsets.all(10.0),
                      child:new RaisedButton
                        (shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),color: Colors.blue[100],
                        onPressed:()
                          {
                            if(nameController.text!='' && descriptionController.text!='' && descriptionController.text.length<20 && nameController.text.length<20)
                            {
                              dialogs.add(new DialogItem(lastMessageTime: '19:19', dialogName:nameController.text,
                                  lastMsgSender: descriptionController.text, lastMsg: '',
                                  dialogPicture: new Image(image: new AssetImage(
                                      'assets/images/avatar3_pch.png'),
                                    height: 70.0,
                                    width: 70.0,), type: DialogItem.CHAT));
                              setState(()
                              {
                                count++;
                              });
                              postDialog(dialogs[count-1]);
                            }
                          //POST THIS SHIT TO SERVER
                            Navigator.pop(ctxt);
                          },
                        padding: EdgeInsets.all(20.0),
                        child: new Text("Создать",
                          style: new TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                          ),
                        ),
                      )
                  ),
                ],
              ),
            ),
          );
      });
  }
}



class DialogItem
{
  String lastMessageTime;
  String dialogName;
  String lastMsgSender;
  String lastMsg;
  Image dialogPicture, typePicture;
  int type;
  String uid;

  static int CHANNEL = 1, CHAT = 2;

  DialogItem({this.lastMessageTime, this.dialogName, this.lastMsgSender, this.lastMsg, this.dialogPicture ,this.type, this.uid})
  {

    if(type==CHANNEL)
      {
        typePicture = new Image(image: new AssetImage('assets/images/tv.png'), height: 20.0, width: 20.0,);
      }
    if(type==CHAT)
    {
      typePicture = new Image(image: new AssetImage('assets/images/chat.png'), height: 20.0, width: 20.0,);
    }
  }

}


