import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter_app/routes/DialogData.dart';
import 'package:flutter_app/routes/user.dart';

class DialogSettingsRoute extends StatefulWidget
{
  List<DialogMember> members;
  String dialogName;
  Image dialogAvatar;
  @override
  _State createState() => new _State(members: members,dialogName: dialogName,dialogAvatar: dialogAvatar);
  DialogSettingsRoute({this.members, this.dialogName,this.dialogAvatar}){}
}



class _State extends State<DialogSettingsRoute>
{
  List<DialogMember> members;
  String dialogName;
  Image dialogAvatar;
  double width;

  _State({this.members, this.dialogName,this.dialogAvatar}){}

  bool overlayShouldBeVisible = false;
  int count = 100;
  bool _switchNotifications = false;
  @override
  void initState()
  {

    setState(()
    {});
    super.initState();

  }

  @override
  Widget build(BuildContext context)
  {
    width = MediaQuery.of(context).size.width;

    return new Scaffold(appBar: new AppBar
      (
      backgroundColor: Colors.blueAccent,
      actions: <Widget>
      [
        new IconButton(icon: const Icon(Icons.more_vert, color: Colors.white,), onPressed: null,),
      ],

      title: new Text('Dialog settings'),
    ),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new Container
            (padding: EdgeInsets.only(top: 20, bottom: 20),color: Colors.grey[100],
            child: new Row(children: <Widget>[Container(padding: EdgeInsets.only(left: 10),child: new Image(image: dialogAvatar.image, width: 80,height: 80,)),
            new Column(children: <Widget>
            [
              Container(padding: EdgeInsets.only(left: width/6),child: new Text(dialogName, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)),
                Container(padding: EdgeInsets.only(left: width/6-30, top: 7),child: new Text(members.length.toString()+' members'))
            ],)],),
            ),
          new Container(padding: EdgeInsets.all(5),),
          new Container(color: Colors.grey[100],padding: EdgeInsets.all(9),child: new Row(children: <Widget>[
            new Text('Notifications', style: TextStyle(fontSize: 18),),
            Expanded( child: Container(alignment: Alignment.centerRight,child: new Switch(value: _switchNotifications, onChanged:  (bool value) { setState(() {_switchNotifications = value;});})))
          ],)),
          new Container(padding: EdgeInsets.all(5),),
          new Expanded
            (
              child: new Container(color: Colors.grey[100],child:
              new ListView.builder(itemCount: members.length+1,
                  itemBuilder: (BuildContext context, int index)
                  {
                    if(index==0)
                      {
                        return new InkWell(onTap: null,child: new Container(padding:EdgeInsets.all(4),child:new Row(children: <Widget>
                        [new IconButton(icon: Icon(Icons.person_add,color: Colors.blue), onPressed: null),
                            new Text('Add member',style: TextStyle(color: Colors.blue),)])));
                      }
                      else
                    {
                      return new Container(padding: EdgeInsets.all(4), child:
                          new Column(children: <Widget>
                          [new Container(padding: EdgeInsets.all(0.1),color: Colors.black,),
                            new Row(children: <Widget>
                              [
                                new Image(image: members[index-1].member.userAvatar.image,
                                  width: 40,
                                  height: 40,),
                                Container(padding: EdgeInsets.only(left: 10,bottom: 9),
                                    child: new Text(members[index-1].member.name)),
                                Container(padding: EdgeInsets.only(left: 10,bottom: 9),
                                    child: new Text
                                      (
                                      members[index-1].isAdmin ? (members[index-1].member.role==User.ROLE_ORG ?'org': 'admin') : '',
                                      style: TextStyle(color: Colors.red[700]),
                                    )),
                                Expanded(child: Container(
                                    alignment: Alignment.centerRight,
                                    child: new IconButton(
                                        icon: Icon(Icons.more_vert), onPressed: ()
                                    =>
                                        ()
                                    {
                                      debugPrint('debug');
                                    })))

                              ],),
                          ],)
                      );
                    }
                  })
              ),
            ),

        ],
      ),
    );
  }
}