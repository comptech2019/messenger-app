import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter_app/routes/DialogData.dart';
import 'package:flutter_app/routes/dialog_settings.dart';
import 'package:flutter_app/routes/user.dart';
import 'package:flutter_app/routes/addQoute.dart';
import 'package:flutter_app/routes/CustomShowDialog.dart';
import 'package:flutter_app/api.dart';
import 'package:flutter_app/routes/channels.dart';
Color primaryColor = new Color(0xff66ccff);


class ChatRoute extends StatefulWidget
{
  DialogData data;
  @override
  _State createState() => new _State(data: data);
  ChatRoute({this.data}){}
}


class ChatMessage extends StatefulWidget
{
  @override
  _MsgState createState() => new _MsgState(text: this.text,senderName: this.senderName,sendTime: this.sendTime,type:this.type, senderAvatar:this.senderAvatar,isQoute: this.isQoute, color: color);

  String text, senderName, sendTime, type;
  Image senderAvatar;
  bool isQoute = false;
  Color color;
  ChatMessage({this.text, this.senderName, this.sendTime,this.type, this.senderAvatar,this.isQoute, this.color});



}

class _MsgState extends State<ChatMessage>{
  int _selected =0;
  String text, senderName, sendTime, type;
  Image senderAvatar;
  bool isQoute = false;
  Color color;
  _MsgState({this.text, this.senderName, this.sendTime,this.type, this.senderAvatar,this.isQoute, this.color});

  void _onChange(int value)
  {
    setState(() {
      _selected = value;
    });
  }
  @override
  Widget build(BuildContext context)
  {
    if(!this.isQoute)
    {
      if (this.type == MessageItem.SENT)
      {
        return new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>
          [
            new Image(image: AssetImage('assets/images/avatar_alpha_pch.png'),
              width: 45,
              height: 45,),
            new Card(
              color: primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: new Padding(
                padding: new EdgeInsets.all(7.0),
                child: new Column
                  (
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>
                  [
                    new Row(children: <Widget>[new Column(
                        children: <Widget>[ new Text(text),]),
                    Container(padding: EdgeInsets.all(7),
                        child: new Text(sendTime, style: TextStyle(
                            color: Colors.blueAccent, fontSize: 13),))
                    ],)
                  ],
                ),
              ),
            ),

          ],
        );
      }
      else if (this.type == MessageItem.RECEIVED)
      {
        return new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>
          [
            new Image(image: senderAvatar.image, width: 45, height: 45, color: color,),
            new Card(
              color: primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: new Padding(
                padding: new EdgeInsets.all(7.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>
                  [
                    new Row(children: <Widget>[new Column(
                        children: <Widget>[
                          new Text(senderName,
                            style: TextStyle(fontWeight: FontWeight.bold, color: color),),
                          new Text(text),
                        ]),
                    Container(padding: EdgeInsets.all(7),
                        child: new Text(sendTime, style: TextStyle(
                            color: Colors.blueAccent, fontSize: 13),))
                    ],)
                  ],
                ),
              ),
            ),

          ],
        );
      }
    }
    else
    {

      return new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>
        [
          new Image(image: AssetImage('assets/images/avatar_alpha_pch.png'),width: 45,height: 45,),
          new Card(
            color: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: new Padding(
              padding: new EdgeInsets.all(7.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  new Text(text,style: TextStyle(fontWeight: FontWeight.bold),),

                  new Row(children: <Widget>[new Text('ДА'), Container(padding:EdgeInsets.only(left: 7),child:new Radio(value: 0, groupValue: _selected, onChanged: (int value){_onChange(value);})),],),
                  new Row(children: <Widget>[new Text('НЕТ'), new Radio(value: 1, groupValue: _selected, onChanged: (int value){_onChange(value);}),],),

                  new Container(
                      padding:EdgeInsets.all(10.0),
                      child:new RaisedButton
                        (shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),color: Colors.blue[100],
                        onPressed:()
                        {},
                        padding: EdgeInsets.all(20.0),
                        child: new Text("Проголосовать",
                          style: new TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                          ),
                        ),
                      )
                  ),
                ],
              ),
            ),
          ),

        ],
      );

    }
  }

}

class _State extends State<ChatRoute>
{

  TextEditingController msgController = new TextEditingController();
  DialogData data;
  _State({this.data}){}
  bool overlayShouldBeVisible = false;




  Future<void> _sendMsg(MessageItem msg) async
  {

    setState(()
    {
      data.msgs.add(msg);
      msgController.text = "";
      if(!msg.isQoute)
        {
          postMsg(msg, data.uid);
        }
      print('sent');
    });

  }

  void _openDialogSettings()
  {
    debugPrint('open');
    List<DialogMember> members = new List();
    for(int i=0; i<20; i++)
      {
        if(i==0)
          {
            members.add(new DialogMember(member: new User(login: 'login_${i}s', name: 'Random User',
                userAvatar: new Image(image: new AssetImage('assets/images/avatar_pch.png'), height: 45.0, width: 45.0,),role: User.ROLE_ORG),isAdmin: true));
          }
        else if(i<3)
          {members.add(new DialogMember(member: new User(login: 'login_${i}s', name: 'Random User',
              userAvatar: new Image(image: new AssetImage('assets/images/avatar_pch.png'), height: 45.0, width: 45.0,),role: User.ROLE_LECTOR),isAdmin: true));}
        else if(i>=3)
          {members.add(new DialogMember(member: new User(login: 'login_${i}s', name: 'Random User',
              userAvatar: new Image(image: new AssetImage('assets/images/avatar_pch.png'), height: 45.0, width: 45.0,), role: User.ROLE_LISTENER),isAdmin: false));}
      }
    Navigator.push(context, MaterialPageRoute(builder: (context) => DialogSettingsRoute(dialogName: data.dialogName, dialogAvatar: data.chatAvatar,
      members: members,)));
  }

  @override
  void initState()
  {
    setState(() {
    });
    super.initState();
  }
  void addQuestion()
  {
    TextEditingController nameController = new TextEditingController();


    showDialog
      (
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return
            new CustomAlertDialog(
              content: new Container(
                width: 260.0,
                height: 250.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: const Color(0xFFFFFF),
                  borderRadius:
                  new BorderRadius.all(new Radius.circular(40.0)),
                ),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    // dialog top
                    new Expanded(
                      child: new Row(
                        children: <Widget>[
                          new Container(alignment: Alignment.topCenter,
                            padding: new EdgeInsets.only(left: 40.0),
                            decoration: new BoxDecoration(
                              color: Colors.white,
                            ),
                            child: new Text(
                              'Создание опроса',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontFamily: 'helvetica_neue_light',
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(padding: EdgeInsets.all(15),),


                    new Container(alignment: Alignment.topCenter,
                      padding:EdgeInsets.all(20.0),decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 350,
                      //color: colorName,
                      child: new TextField(
                        onChanged: null,
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                        ),controller: nameController,
                        decoration: new InputDecoration.collapsed(
                            hintText: 'Заголовок'
                        ),
                      ),
                    ),


                    Container(padding: EdgeInsets.all(10),),
                    new Container(
                        padding:EdgeInsets.all(10.0),
                        child:new RaisedButton
                          (shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),color: Colors.blue[100],
                          onPressed:()
                          {
                            if(nameController.text!='' && nameController.text.length<20)
                            {
                              _sendMsg(new MessageItem(
                                  text: nameController.text,
                                  senderName: currentUser.name,
                                  sendTime: "19:19",
                                  senderAvatar: new Image(image: AssetImage('assets/images/avatar_alpha_pch.png')),
                                  type: MessageItem.SENT,isQoute: true));
                            }
                            Navigator.pop(context);
                          },
                          padding: EdgeInsets.all(20.0),
                          child: new Text("Создать",
                            style: new TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        )
                    ),
                  ],
                ),
              ),
            );
        });
    print('qoute');
  }
  void addHere(){print('here');}

  @override
  Widget build(BuildContext context)
  {
    int iterator=0;
      return new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Scaffold(
            appBar: new AppBar
              (
                backgroundColor: Colors.blueAccent,
                actions: <Widget>
                [
                  new IconButton(
                    icon: const Icon(Icons.more_vert, color: Colors.white,),
                    onPressed: null,),
                ],

                title: Container(child:
                InkWell(onTap: ()
                => _openDialogSettings(), child: new Row(children: <Widget>
                [data.chatAvatar,
                  Container(padding: EdgeInsets.only(left: 20, top: 8),
                      child: new Column(children: <Widget>[
                        new Text(data.dialogName),
                        new Text(data.membersCount.toString() + ' members',
                          style: TextStyle(fontSize: 12,
                              color: Colors.white,
                              fontStyle: FontStyle.italic),)
                      ],))
                ])),

                )),
            body:
            new Stack(children: <Widget>[
              new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Expanded(
                    child: new Container(child:
                    new ListView.builder(itemCount: data.msgs.length,
                        itemBuilder: (BuildContext context, int index)
                        {
                          Image ava = data.msgs[index].senderAvatar;
                          if (index < data.msgs.length - 1 && data.msgs[index].senderName == data.msgs[index + 1].senderName && !data.msgs[index+1].isQoute)
                          {
                            ava = new Image(image: AssetImage(
                                'assets/images/avatar_alpha_pch.png'),
                              width: 45,
                              height: 45,);
                          }
                          if(index > 0 && data.msgs[index].senderName != data.msgs[index -1].senderName && !data.msgs[index-1].isQoute)
                            {
                                iterator++;
                            }
                            Color cc = Colors.blue;
                            if(iterator%4==0)
                              {
                                cc = Colors.blue;
                              }
                            else if((iterator+1)%4==0)
                            {
                              cc = Colors.red;
                            }
                            else if((iterator+2)%4==0)
                            {
                              cc = Colors.orange[400];
                            }
                            else if((iterator+3)%4==0)
                            {
                              cc = Colors.green[400];
                            }
                          return new ChatMessage(text: data.msgs[index].text,
                              senderName: data.msgs[index].senderName,
                              sendTime: data.msgs[index].sendTime,
                              senderAvatar: ava,
                              type: data.msgs[index].type,isQoute: data.msgs[index].isQoute?true:false, color: cc,);
                        })
                    ),
                  ),
                  new Container(
                    height: 60.0,
                    color: Colors.white,
                    child: new Row
                      (
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[

                        new Expanded(
                          child: new Padding(
                            padding: new EdgeInsets.only(left: 70.0),
                            child: new TextField(controller: msgController,
                                keyboardType: TextInputType.multiline,
                                maxLines: 30,
                                decoration: new InputDecoration(
                                  hintText: 'Input your message',)),
                          ),
                        ),
                        new Material(
                          color: Colors.white,
                          child: new InkWell(
                            child: new Padding(
                              padding: new EdgeInsets.symmetric(
                                  horizontal: 20.0),
                              child: new Icon(
                                Icons.send,
                                color: Colors.blue,
                              ),
                            ),
                            onTap: ()
                            =>
                                _sendMsg(new MessageItem(
                                    text: msgController.text,
                                    senderName: currentUser.name,
                                    sendTime: "19:19",
                                    senderAvatar: currentUser.userAvatar,
                                    type: MessageItem.SENT,isQoute: false)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              new Container(
                child: currentUser.role!=User.ROLE_LISTENER?new AddQoute(addQoute: addQuestion, addIAmHere: addHere,):null,
                padding: EdgeInsets.only(left: 2, bottom: 2),),
            ],),
          ),
          //overlayShouldBeVisible == true ? new JsonLoader(): new Container(),
          //Library.debugMode ? new DebugOverlay(): new Container(),
        ],
      );

  }
}


