import 'package:flutter/material.dart';
import 'package:flutter_app/api.dart';

class profileView extends StatefulWidget
{
  @override
  _State createState() => new _State();

}
class _State extends State<profileView>
{
  String firstName="";
  String lastName="";
  String cfName="";
  String clName="";
  List<String> news;
  List<String> achievment;
  void getFromBD()
  {
    firstName=currentUser.name;
    //lastName="NotName";
    news = List<String>.generate(100, (i) => "Новостная лента, здесь будет информация различного рода $i");
    achievment=new List();
    for (int i=0;i<9;i++)
      {
        achievment.add("BestCoder 200$i ");
      }
  }
  void changeFirstName(String str)
  {
    setState(() {

    });
    cfName=str;
  }
  /*void changeLastName(String str)
  {
    setState(() {

    });
    clName=str;
  }*/
  void submit()
  {
    setState(() {

    });
    firstName=cfName;
    //lastName=clName;
    Navigator.of(context).pop();
  }
  void _showDialog() {
   // getNameFromBD();
    // flutter defined function
    showDialog
      (
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return 
          AlertDialog(
          title: new Text("Сменить имя"),
          content: new Column
            (
              //mainAxisSize: MainAxisSize.min,
              children:<Widget>
              [
            new TextField(
              onChanged: changeFirstName,
              style: TextStyle(
                fontSize: 16,
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '$firstName',
                labelText: 'Имя',
            ),),
            /*new TextField(
              onChanged: changeLastName,
              style: TextStyle(
                fontSize: 16,
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '$lastName',
                labelText: 'Фамилия',
            ),),*/
          ]),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: submit,
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    getFromBD();
    return new Scaffold(
      appBar: new AppBar
        (
        backgroundColor: Colors.blueAccent,
        title: new Text('User profile'),
      ),
      body: new Container(child: new Column(children: <Widget>[
          new Expanded(child:new Container(
            //alignment: Alignment.centerLeft,
          child: ListView.builder(
    itemCount: news.length+achievment.length+1,

    itemBuilder: (context, index)
    {
      if(index==0)
        {
          return (new Row(
              children: <Widget> [
                new Container
                  (
                  width: 130.0,
                  height: 130.0,
                  decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                          fit: BoxFit.contain,
                          image: currentUser.userAvatar.image
                      )
                  ),
                ),
                new Container(
                    padding: EdgeInsets.all(20.0),
                    child: new Column(
                        children: <Widget> [

                          new Center(
                              child: new RaisedButton(
                                  color: Colors.white,
                                  onPressed: _showDialog,
                                  child:new Text('$firstName',textAlign: TextAlign.center,style:TextStyle(
                                    fontSize: 32,
                                  )))),
                          /*new Center(
                              child:new RaisedButton(
                                  color: Colors.white,
                                  onPressed: _showDialog,
                                  child: new Text("$lastName",textAlign: TextAlign.center,style:TextStyle(
                                    fontSize: 32,
                                  )))),*/
                        ]
                    )


                )
              ]
          ));
        }


    }
    ),
            )
          ),
          ])
      )
    );
  }
}