import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  TextEditingController firstName;
  TextEditingController lastName;
  TextEditingController pass1;
  TextEditingController pass2;

  bool obscure = true;
  bool confirmRegister()
  {
    setState(() {

    });
    RegExp exp = new RegExp(r"(\w+)");
    //Iterable<Match> matches = exp.allMatches(str);
      if(firstName.text == "" || lastName.text == "")
      {
        return false;
      }
      else if(pass1.text.length < 7)
      {
        return false;
      }
      else if(pass1.text != pass2.text)
      {
        return false;
      }
    return true;
  }

  void showPass(bool ok)
  {
    setState(() {

    });
    obscure = !ok;
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Регистрация'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(8.0)
            ),
            Image.asset(
              'images/logo.jpg',
              width: 150,
              height: 82.5,
            ),
            Padding(
                padding: EdgeInsets.all(8.0)
            ),
            new Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),width: 360, child: Text(
              'Регистрация',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
            )),
            Container(padding: EdgeInsets.all(7),),
            Container(padding: EdgeInsets.all(15),decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 360, child:
            TextFormField(
              decoration: InputDecoration.collapsed(hintText: 'Ваше имя'), controller: firstName,
            )),
            Container(padding: EdgeInsets.all(7),),
            Container(padding: EdgeInsets.all(15),decoration: BoxDecoration(color: Colors.grey[200],borderRadius: BorderRadius.circular(20)),width: 360, child:
            TextFormField(
              decoration: InputDecoration.collapsed(
                  hintText: 'Ваша фамилия'
              ),
              controller: lastName,
            )),
            Padding(
                padding: EdgeInsets.all(8.0)
            ),
            Container(padding: EdgeInsets.all(15),decoration: BoxDecoration(color: Colors.grey[300],borderRadius: BorderRadius.circular(20)),width: 360, child:TextFormField(
              decoration: InputDecoration.collapsed(
                  hintText: 'Ваш пароль'
              ),
              obscureText: obscure,
              controller: pass1,
            )),
            Container(padding: EdgeInsets.all(7),),
            Container(padding: EdgeInsets.all(15),decoration: BoxDecoration(color: Colors.grey[300],borderRadius: BorderRadius.circular(20)),width: 360, child:TextFormField(
              decoration: InputDecoration.collapsed(
                  hintText: 'Подтвердите ваш пароль'
              ),
              obscureText: obscure,
              controller: pass2,
            )),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                  Checkbox(
                    value: !obscure,
                    onChanged: showPass,
                  ),
                  Text(
                    'Показать пароли',
                  ),
                  Spacer(

                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: confirmRegister,
        tooltip: 'Завершить регистрацию',
        child: Icon(Icons.check_box),
      ),
    );
  }
}