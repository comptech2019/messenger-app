import 'package:flutter/material.dart';

class User
{

  String name, login, password;
  Image userAvatar;
  int role;
  String uid;
  static final int ROLE_ORG=1, ROLE_LISTENER=2, ROLE_LECTOR=3;

  User({this.login,this.name,this.userAvatar, this.role, this.uid});

}
class DialogMember
{
  User member;
  bool isAdmin;
  DialogMember({this.member, this.isAdmin}){}
}